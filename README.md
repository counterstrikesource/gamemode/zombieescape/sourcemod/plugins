# Plugins

Repository used to compile specific plugins that will be used for a source game mode

## Development

### Install docker and docker-compose

[Docker installation](https://docs.docker.com/engine/install/ubuntu/)
[Docker compose installation](https://docs.docker.com/compose/install/)

Don't forget to add yourself into the docker group to run docker commands without using "sudo"

```sudo groupadd docker; sudo usermod -aG docker $USER; newgrp docker```

### Clone the plugins repository

```git clone https://gitlab.com/counterstrikesource/plugins.git -b ze --depth 1 --recursive```

### Run into the container
```docker-compose up -d && docker exec -it plugins-dev bash```

### Copy include files and spcomp

```cp include/ -R ../plugins_css/ && cp spcomp ../plugins_css/```

### Copy independent plugin folders

```cp -R hlstatsx ../plugins_css && cp -R sourcebanspp ../plugins_css && cp -R zombiereloaded ../plugins_css```

### Go into your current folder

```cd ../plugins_css```

### Build your plugin

```python3 compile.py zombiereloaded```

### Generated .smx

They will be located under the folder name "plugins"
